const http = require('http');
const express = require('express');

const host = process.env.HOSTNAME || 'localhost';
const port = process.env.PORT || 8080;

const app = express();
const httpServer = http.createServer(app);
//const secretToken = 'cantguessthis';

const delayTimes = {
  login: 30,
  checkCreds: 90,
  generateRefreshToken: 20,
  checkRefreshToken: 15,
  generateAccessToken: 12,
  accesstoken: 80,
  data: 50,
};

const users = [
  {
    username: 'kant',
    password: 'aufklaerung',
  },
  {
    username: 'platon',
    password: 'idee',
  },
  {
    username: 'sokrates',
    password: 'dontknow',
  },
];
const refreshTokens = [];
const accessTokens = {};


app.use(express.json());



// basis route die das html frontend liefert
app.use('/', express.static(__dirname + '/wwwPublic'));



/**
 * login route
 * überprüft username und passwort
 * generiert refreshtoken
 * antwortet bei korrektem user/pwd mit refreshtoken
 */
app.use('/login', (req, res) => {

    console.log(`login API route:`)
    console.log(req.body)
   
    setTimeout(() => {
        checkCredentials(req.body).then(user => {
            generateRefreshToken(user).then(token => {
                res.status(200);
                res.setHeader('content-type', 'text/plain');
                res.send(token);
            }).catch(err => res.statusCode(401).send(err));
        }).catch(err => res.status(401).send(err));
    }, delayTimes['data']);
});

/**
 * accesstoken route
 * überprüft refreshtoken (welches nur nach erfolgreichem login vorhanden sein darf)
 * generiert aus dem refreshtoken ein accesstoken für den datenzugriff
 * antwortet bei korrektem refreshtoken mit dem accesstoken
 */
app.use('/accesstoken', (req, res) => {
    console.log(`accesstoken API route:`)
    console.log(req.headers['authorization'])

    setTimeout(() => {
        let sentRefreshToken = req.headers['authorization'];  // das notwendige refreshtoken kommt im header daher (weil wir es in der client.js auch so wegschicken)
        checkRefreshToken(sentRefreshToken)
        .then(() => {
            generateAccessToken(sentRefreshToken)
            .then(aToken => {
                res.status(200);
                res.setHeader('content-type', 'text/plain');
                res.send(aToken);
            }).catch(err => res.status(401).send(err));
        }).catch(err => res.status(401).send(err));
    }, delayTimes['accesstoken']);
});



/**
 * data route
 * erwartet ein korrektes accesstoken 
 * antwortet mit den verlangten daten (dies ist in dem fall einfach nur ein text, kannn aber natürlich auch aus einer datenbank kommen usw.)
 */
app.use('/data', (req, res) => {
    console.log(`data API route:`)
    console.log(req.headers['authorization'])

    setTimeout(() => {
        let accessToken = req.headers['authorization'];
        if (Object.values(accessTokens).includes(Number(accessToken))) {
            res.status(200);
            res.send('here is your secret data ;-)');
        } 
        else {
            res.status(401);
            res.send('Unauthorized');
        }
    }, delayTimes['data']);
});




//nützliche funktionen (überprüfe logindaten, generiere refreshtoken, generiere accesstoken, check refreshtoken)
function checkCredentials(credentials) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let user = users.find(
        (u) =>
          u.username === credentials.username &&
          u.password === credentials.password,
      );

      if (user) resolve(user);
      else reject('user not found');
    }, delayTimes['checkCreds']);
  });
}

function generateRefreshToken(user) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let rToken = refreshTokens.find((t) => t.username === user.username);
      if (!rToken) {
        rToken = { username: user.username };
        refreshTokens.push(rToken);
      }
      rToken.token = Math.floor(Math.random() * 10000);
      accessTokens[rToken.token] = null;

      resolve(rToken.token.toString());
    }, delayTimes['generateRefreshToken']);
  });
}

function checkRefreshToken(rToken) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Object.keys(accessTokens).includes(rToken)) resolve();
      else reject('refresh token not found');
    }, delayTimes['checkRefreshToken']);
  });
}

function generateAccessToken(rToken) {
  return new Promise((resolve) => {
    setTimeout(() => {
      accessTokens[rToken] = Math.floor(Math.random() * 1000) + 10000;
      resolve(accessTokens[rToken].toString());
    }, delayTimes['generateAccessToken']);
  });
}




// startet den express server
httpServer.listen(port, () =>
  console.log(`Client running at http://${host}:${port}`),
);
