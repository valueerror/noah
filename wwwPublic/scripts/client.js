let logger = null;

$(() => {
    logger = createLogger('#log');
    setup();
});

function setup() {
    $('#btn00').on('click', () => getDataWithAccessToken().then(data => logger.success(`received data: ${data}`)).catch((message) => logger.error(message)));
    $('#btn01').on('click', () => getData('ok').then(data => logger.success(`received data: ${data}`)).catch((message) => logger.error(message)));
    $('#btn02').on('click', () => getData('fail1').then(data => logger.success(`received data: ${data}`)).catch((message) => logger.error(message)));
    $('#btn03').on('click', () => getData('fail2').then(data => logger.success(`received data: ${data}`)).catch((message) => logger.error(message)));
    $('#btn04').on('click', () => getData('fail3').then(data => {logger.success(`received data: ${data}`)}).catch((message) => { logger.error(message)  }));
    $('#btn06').on('click', clearTokens);
}



/**
 * the global fetch() method that provides an easy, logical way to fetch resources asynchronously across the network.
 * This kind of functionality was previously achieved using XMLHttpRequest.
 * Fetch provides a better alternative that can be easily used by other technologies such as Service Workers. 
 * 

    fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        })
        .then(response => response.json())
        .then(data => {
            console.log("do whatever you want with the data")
            console.log(data)
        });


*/













/** 
* myFetch ist der versuch das xmlHTTPrequest auszulagern in eine eigene funktion 
* um den code etwas klarer zu gliedern
* FIXME !  der code is dysfunktional derweil ;-)
*/
function myFetch(method, url, headers){
    return new Promise((resolve,reject) => {
       
        const getDataCall = new XMLHttpRequest();
       
        getDataCall.onreadystatechange = function () {
            if (this.readyState == 4) {
                if ((this.status >= 200) && (this.status <= 299)) {
                    //let data = getDataCall.responseText;
                    resolve(this);
                } else {
                    reject(this);
                }
            } 
        };

        getDataCall.open(method, url, true);
        
        // for (let header in headers){
        //     getDataCall.setRequestHeader('authorization', accessToken);
        // }
        getDataCall.send();
    });
}





function getData(type) {
    return new Promise(function (resolve, reject) {
        logger.log(`---------------------------------------`);

        // get username and password with jquery
        let credentials = {
             username: $('#username').val(),
             password: $('#password').val(),
        };

        // wir simulieren hier dass das passwort falsch eingegeben wurde in dem wir vor dem logincall das password falsch machen
        if ( type === 'fail3') {
            logger.log(`manipulating password!`);
            credentials.password = 'not correct';
        }

  

       /** 
        * myFetch ist der versuch das xmlHTTPrequest auszulagern in eine eigene funktion 
        * um den code etwas klarer zu gliedern
        * FIXME !
        */

        // myFetch('POST', '/login', {'Content-Type': 'application/json'}, JSON.stringify(credentials))
        // .then(response => {
        //     let refreshToken = response.responseText;
        //     logger.success(`received refreshToken: ${refreshToken}`);

        //     if (type === 'fail1') refreshToken += 1000;
        //     storeToken ('refreshToken', refreshToken);
        // });


        const loginCall = new XMLHttpRequest();
        loginCall.onreadystatechange = function () {
           
            if (this.readyState == 4) {  //login api route hat status 200 (alles ok) und ein token als responseText zurückgesandt
                if (this.status == 200) {
                    // Typical action to be performed when the document is ready:
                    let refreshToken = loginCall.responseText;
                    logger.success(`login ok`);
                    logger.success(`received refreshToken: ${refreshToken}`);

                    /**
                     * wir simulieren hier ein falsches refreshtoken indem wir es hier verändern
                     * kurz bevor wir es in die session storage schreiben 
                     */
                    if (type === 'fail1') {
                        logger.log(`manipulating refreshToken!`);
                        refreshToken += 100000;
                    }
                    storeToken('refreshToken', refreshToken);   //speichere das refreshtoken im sessionstorage (browser cache quasi)


                    //jetzt nachdem wir eingelogged sind und ein refreshtoken erhalten haben können wir accesstoken holen um später daten abgreifen zu dürfen
                    const accessTokenCall = new XMLHttpRequest();
                    accessTokenCall.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            if (this.status == 200) {
                          
                                let accessToken = accessTokenCall.responseText;
                                logger.success(`received accessToken: ${accessToken}`);

                                /**
                                 * wir simulieren hier ein falsches ACCSESS token indem wir es hier verändern
                                 * kurz bevor wir es in die session storage schreiben 
                                 */
                                if (type === 'fail2') {
                                    logger.log(`manipulating accessToken!`);
                                    accessToken += 100000;
                                }
                                storeToken('accessToken', accessToken);   // wenn der accesstokencall erfolgreich war speichrn wir das accestoken für späteren zugriff auf die daten api
                                

                                // OPTIONAL! alles korrekt?  holen wir uns mal ein paar daten
                                getDataWithAccessToken().then(data => {
                                    logger.log(data)
                                })   

                            }
                            else {
                                logger.error(`invalid refreshtoken - getting accesstoken failed`); 
                            }
                        }
                    }
                    // diesmal geht es um authorization daher senden wir das erhaltene refreshtoken nicht um body sondern im header unter "authorization" mit
                    accessTokenCall.open('POST', '/accesstoken', true);
                    accessTokenCall.setRequestHeader('Authorization', `${refreshToken}` );
                    accessTokenCall.send();
                }
                else {
                    logger.error(`invalid cedentials - login failed`); 
                }
            }        
        }
        loginCall.open('POST', '/login', true);
        loginCall.setRequestHeader('Content-type', 'application/json');
        loginCall.send(JSON.stringify(credentials));
    });
}





function getDataWithAccessToken() {
    return new Promise((resolve, reject) => {
        logger.log(`---------------------------------------`);

        const getDataCall = new XMLHttpRequest();
        getDataCall.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    let data = getDataCall.responseText;
                    resolve(data);
                } else {
                    logger.error("invalid accesstoken - get data failed");
                }
            }
        };

        getDataCall.open('GET', '/data', true);
        let aToken = getToken('accessToken');  // wir holen das zuvor gespeicherte accestoken aus der sessionstorage. wird benötigt sonst gibt uns die data api nix zurück
        getDataCall.setRequestHeader('authorization', aToken);
        getDataCall.send();
    });
}






/** 
 * helper functions  
 * */
function storeToken(tokenType, token) {
     sessionStorage.setItem(tokenType, token);
}

function getToken(tokenType) {
    console.log(sessionStorage)
    return sessionStorage.getItem(tokenType);
}

function clearTokens() {
    logger.log(`accessToken and refreshToken removed from sessionStorage`);
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('refreshToken');
}
